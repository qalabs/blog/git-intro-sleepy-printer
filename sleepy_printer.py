import string
import time

def print_and_sleep(character, interval=1):
    print(character)
    time.sleep(interval)

def print_digits():
    for digit in string.digits:
        print_and_sleep(digit, 0.75)

def print_letters():
    for letter in string.ascii_letters:
        if letter.islower():
            print_and_sleep(letter, 0.5)
        else:
            print_and_sleep(letter, 0.25)

print_digits()
print_letters()
